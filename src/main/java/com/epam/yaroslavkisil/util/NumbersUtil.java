package com.epam.yaroslavkisil.util;

import java.util.Comparator;
import java.util.function.IntPredicate;
import java.util.stream.IntStream;

public class NumbersUtil {
    /**
     * in this class i m made comparator&getters and setters for all methods
     *
     */
    public static final IntPredicate isOdd = n -> n % 2 != 0;
    public static final IntPredicate isEven = n -> n % 2 == 0;


    public static int[] determineOdd(int start, int end) {
        return IntStream.range(start, end + 1)
                .filter(isOdd)
                .toArray();
    }

    public static int[] determineEven(int start, int end) {
        return IntStream.range(start, end + 1)
                .filter(isEven)
                .boxed()
                .sorted(Comparator.reverseOrder())
                .mapToInt(m -> m)
                .toArray();
    }

    public static int[] filter(int[] array, IntPredicate filterPredicate) {
        return IntStream.of(array)
                .filter(filterPredicate)
                .toArray();
    }

    public static int getSum(int[] array) {
        return IntStream.of(array).sum();
    }

    public static int getMax(int[] array) {
        return IntStream.of(array)
                .max().getAsInt();
    }

    public static int getMin(int[] array) {
        return IntStream.of(array)
                .min().getAsInt();
    }

    public static double calculateAverage(int[] array) {
        return IntStream.of(array)
                .summaryStatistics().getAverage();
    }
}