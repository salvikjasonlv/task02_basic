package com.epam.yaroslavkisil;

import com.epam.yaroslavkisil.client.TerminalClient;

/**
 * its a main method to launch program
 */
public class Aplication {
    public static void main(String[] args) {
        TerminalClient client = new TerminalClient();

        System.out.println("********* Interval app *********");
        client.runInterval();

        System.out.println("********* Fibonacci app *********");
        client.fibonacciApp();
    }
    }


