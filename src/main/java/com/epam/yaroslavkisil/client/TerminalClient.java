package com.epam.yaroslavkisil.client;

import com.epam.yaroslavkisil.util.FibonacciUtil;
import com.epam.yaroslavkisil.util.NumbersUtil;

import java.io.Console;
import java.util.Arrays;
import java.util.Scanner;

import static com.epam.yaroslavkisil.util.FibonacciUtil.*;
import static com.epam.yaroslavkisil.util.NumbersUtil.*;

public class TerminalClient {


    public TerminalClient() {
    }

    /**
     * in this method we ask abut numbers and check it
     */
    public void runInterval() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter the start of range (starts from 1): ");
        int start = scanner.nextInt();

        if (start < 1) {
            System.out.print("Range cannot starts from 0, try again:");
            start = scanner.nextInt();
        }

        System.out.println();
        System.out.print("Enter the end of range(inclusive): ");
        int end = scanner.nextInt();
        if (end > 100) {
            System.out.println("End range cannot be more that 100, try again:");
            end = scanner.nextInt();
        }

        System.out.println(

        );
        String odd = Arrays.toString(determineOdd(start, end));
        System.out.println(String.format("Odd numbers: %s", odd));

        String even = Arrays.toString(determineEven(start, end));
        System.out.println(String.format("Even numbers: %s", even));
    }

    public void fibonacciApp() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter size of  generated fibonacci nubmers (starts from 1): ");
        int size = scanner.nextInt();

        int[] fib = calculate(size);
        int[] odd = filter(fib, isOdd);
        int[] even = filter(fib, isEven);

        int maxOdd = getMax(odd);
        int maxEven = getMax(even);

        System.out.println("Fibonacci sequence: " + Arrays.toString(fib));
        System.out.println("Max odd: " + maxOdd);
        System.out.println("Max even: " + maxEven);
        System.out.println("Average odd: " + calculateAverage(odd));
        System.out.println("Average even: " + calculateAverage(even));

    }


}